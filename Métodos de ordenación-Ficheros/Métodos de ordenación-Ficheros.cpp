// M�todos de ordenaci�n-Ficheros.cpp: define el punto de entrada de la aplicaci�n de consola.
//

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

const string nombreFichero = "db.txt";
int Cont = 0;

class Usuarios
{
	public:
		///Este m�todo pide los datos nuevos de un usuario a ingresar
		///limpia el bufer (por las cadenas que se toman)
		///y crea un archivo o en caso de que exista, s�lo lo abre para escritura.
		void Usuarios::agregarUsuarios()
		{
			//Escribir en el fichero
			cout << "Ingresa el registro del usuario(numeros): ";
			cin>> registro;

			cin.ignore(265,'\n');
			cout << "Ingresa el nombre del usuario: ";
			getline(cin, nombre);

			ofstream escribir(nombreFichero, ios::app);
			escribir << registro << " " << nombre << endl;
			escribir.close();
		}

		///Esteblece el tama�o del arreglo
		///algo as� como la propiedad count de una lista.
		/*void setSizeArray()
		{
			int buscarElemento;
			for (int i = 0; i < 150 ; i++)
			{
				buscarElemento = arreglo[i];
				if (buscarElemento <=0)
				{
					i = 150;
				}
				else
				{
					SizeArray=i;
				}
			}
		}*/

		///Le da valores al arreglo toma s�lo los
		///Registros que est�n guardados en el fichero.
		/// Y establece el tama�o del arreglo
		void Usuarios::setValuesAndSizeArray()
		{
			char splitCharacters[10];
			string value = "";
			int i = 1;
			ifstream recibir(nombreFichero);

			recibir >> arreglo[0];
			while (!recibir.eof())
			{
				try
				{
					recibir >> splitCharacters;
					//splitCharacters[17] = ;
					if (splitCharacters[0] == '0' || splitCharacters[0] == '1' || splitCharacters[0] == '2' || 
						splitCharacters[0] == '3' || splitCharacters[0] == '4' || splitCharacters[0] == '5' || 
						splitCharacters[0] == '6' || splitCharacters[0] == '7' || splitCharacters[0] == '8' || 
						splitCharacters[0] == '9')
					{
						arreglo[i] = std::stoi(splitCharacters);
						i++;
						SizeArray = i;
					}
				}
				catch (const std::exception&)
				{

				}
				/*if (value == "\n")
				{
					arreglo[i] = std::stoi(value);
					i++;
				}*/

			}
			recibir.close();

		}

		void Usuarios::mostrarArreglo()
		{
			for (int i = 0; i <SizeArray; i++)
			{
				if (arreglo[i] > 0)
				{
					cout << "\n" << arreglo[i];
				}
			}
		}

		void Usuarios::ordenamientoBurbuja()
		{
			setValuesAndSizeArray();
			//setSizeArray();

			int temporal;
			for (int i = 0; i<SizeArray; i++)
			{
				for (int o = 0; o < SizeArray; o++)
				{

					if (o != SizeArray - 1)
					{
						if (arreglo[o] > arreglo[o + 1])
						{
							temporal = arreglo[o];
							arreglo[o] = arreglo[o + 1];
							arreglo[o + 1] = temporal;
						}
					}

				}
			}
		}

		void Usuarios::ordenamientoShell()
		{
			setValuesAndSizeArray();

			int salto = (SizeArray + 1) / 2;
			int sw;
			int auxi = 0;
			int posicion = 0;
			while (salto > 0)
			{
				sw = 1;
				while (sw != 0)
				{
					sw = 0;
					posicion = 1;
					while (posicion <= ((SizeArray + 1) - salto))
					{
						if (arreglo[posicion - 1] > arreglo[(posicion - 1) + salto])
						{
							auxi = arreglo[(posicion - 1) + salto];
							arreglo[(posicion - 1) + salto] = arreglo[posicion - 1];
							arreglo[(posicion - 1)] = auxi;
							sw = 1;
						}
						posicion++;
					}
				}
				salto = salto / 2;
			}
		}

		void Usuarios::ordenamientoInserccion()
		{
			setValuesAndSizeArray();

			int posicion = 0, temporal = 0;

			for (int i = 1; i < SizeArray; i++)
			{
				temporal = arreglo[i];
				posicion = i;
				for (int it = 1; temporal < arreglo[posicion - 1]; posicion--)
				{
					arreglo[posicion] = arreglo[posicion - 1];
				}
				arreglo[posicion] = temporal;
			}
		}


		void Usuarios::controladorOrdenamientoQuickSort()
		{
			setValuesAndSizeArray();
			metodoQuick(arreglo, 0, SizeArray);
		}

		void Usuarios::metodoQuick(int arr[], int primero, int ultimo)
		{
			int i, j, x, aux;
			i = primero;
			j = ultimo;
			x = arr[(primero + ultimo) / 2];
			do {
				while ((arr[i] < x) && (j <= ultimo))
				{
					i++;
				}

				while ((x < arr[j]) && (j > primero))
				{
					j--;
				}

				if (i <= j)
				{
					aux = arr[i]; arr[i] = arr[j]; arr[j] = aux;
					i++;  j--;
				}

			} while (i <= j);

			if (primero < j)
				metodoQuick(arr, primero, j);
			if (i < ultimo)
				metodoQuick(arr, i, ultimo);
		}


		void Usuarios::controladorMerge()
		{
			setValuesAndSizeArray();
			ordenarMezcla(arreglo,0,SizeArray);
		}

		void Usuarios::mezclar(int A[], int izq, int der, int centro)
		{
			int i = izq;
			int j = centro + 1;
			int* aux = new int[der - izq + 1];
			int k = 0;

			while ((i <= centro) && (j <= der))
			{
				Cont++;
				if (A[i]<A[j])
				{
					aux[k] = A[i];
					i++;
				}
				else
				{
					aux[k] = A[j];
					j++;
				}
				k++;
			}

			while (i <= centro)
			{
				Cont++;
				aux[k] = A[i];
				i++;
				k++;
			}

			while (j <= der)
			{
				Cont++;
				aux[k] = A[j];
				j++;
				k++;
			}

			for (i = 0; i<k; i++)
			{
				Cont++;
				A[i + izq] = aux[i];
			}

		}

		void Usuarios::ordenarMezcla(int A[], int p, int q)
		{
			Cont++;
			int centro = 0;

			if (p<q)
			{

				centro = (p + q) / 2;
				ordenarMezcla(A, p, centro);
				ordenarMezcla(A, centro + 1, q);
				mezclar(A, p, q, centro);
			}
		}
		
		


	private:
		int SizeArray;
		string registro = "", nombre = "";
		int arreglo[];

};

int main()
{
	Usuarios usuario;  //Instancia de objeto
	int optionSwitch = 10, opcionMetodo = 10, opcionMostrar=2;
	string repetirWhile = "";

	do
	{
		system("cls");
		cout << "Que quieres hacer?\n"
				<< "1)Agregar un usuario\n"
				<< "2)Ordenar los usuarios\n"
			 << "Opcion: ";

		cin >> optionSwitch;

#pragma region opciones

		switch (optionSwitch)
		{
			case 1:
				usuario.agregarUsuarios();
				break;

			case 2:
				cout << "\t\t\t\t\tMetodos de ordenacion\n\n"
						<< "1)Burbuja\n"
						<< "2)Inserccion\n"
						<< "3)Quicksort\n"
						<< "4)Shell\n"
						<< "5)Merge\n"
					<<"Opcion: ";

				cin >> opcionMetodo;

				switch (opcionMetodo)
				{
					case 1:
						usuario.ordenamientoBurbuja();
						break;

					case 2:
						usuario.ordenamientoInserccion();
						break;

					case 3:
						usuario.controladorOrdenamientoQuickSort();
						break;

					case 4:
						usuario.ordenamientoShell();
						break;

					case 5:
						usuario.controladorMerge();
						break;

					default:
						break;
				}
				cout << "\n\n\nQuieres imprimir el arreglo ordenado?\n"
					    << "1)Si\n"
					    << "2)No\n"
					 <<"Opcion: ";
				cin.ignore(265,'\n');
				cin >> opcionMostrar;

				if (opcionMostrar == 1)
				{
					usuario.mostrarArreglo();
				}
				
				break;

			default:
				break;
		}
#pragma endregion

		cout << "\n\nQuieres volver al menu de inicio? S/N :";
		cin >> repetirWhile;

	} while (repetirWhile == "s");

	cout << endl << "\n\n\n\n";
	system("pause");
    return 0;
}

